/*
    verticalboard
    Copyright (C) 2020  Taco Steemers  tacosteemers.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
*/

var saveTimer;
var articleCounter = -1;
var colorForActiveElements = "lightgrey";
var colorForInactiveElements = "white";
var numColumns = 6;

function init() {
    if (typeof(Storage) !== "undefined") {
        updateRemainingSpaceInformation();

        // Start periodical save action.
        saveTimer = setInterval("saveEverything()", 1000);

        loadFromLocalStorage();

        if (articleCounter == -1) {
            addNewEntry("s1TextAreaContainer");
            document.getElementById("article0textarea").value = "A board for organising your thoughts and TODOs for the day. Each vertical is one category or stage. Each element can be moved to another vertical. Can be used as a Kanban board.";
        }

        document.getElementById('file').addEventListener('change', loadBackup, false);
    } else {
        let span = document.createElement("span");
        span.innerHTML = "Unfortunately your browser does not have storage support. That means your session cannot be stored; whatever you fill in will be lost at some point in time.";
        document.getElementById("pageheader")
        pageheader.appendChild(span);
    }
}

function loadFromLocalStorage() {
    articleCounter = localStorage.getItem("articleCounter");
    if (articleCounter === null || articleCounter === undefined) {
        articleCounter = -1;
        store("articleCounter", articleCounter);
        return;
    }
    let i;
    for (i = 0; i <= articleCounter; i++) {
        let parentIdKey = "article" + i + "section";
        if (localStorage.getItem(parentIdKey) == null) {
            continue;
        }
        let textareaContentsKey = "article" + i + "textarea";
        addNumberedEntry(localStorage.getItem(parentIdKey), i);
        document.getElementById(textareaContentsKey).value = localStorage.getItem(textareaContentsKey);
    }
    for (i = 1; i < 1 + numColumns; i++) {
        let inputIdMaxArticles = "s" + i + "MaxArticles";
        if (localStorage.getItem(inputIdMaxArticles) == null) {
            continue;
        }
        document.getElementById(inputIdMaxArticles).value = localStorage.getItem(inputIdMaxArticles);
        let inputIdTitle = "s" + i + "Title";
        if (localStorage.getItem(inputIdTitle) == null) {
            continue;
        }
        document.getElementById(inputIdTitle).value = localStorage.getItem(inputIdTitle);
    }
}

function saveEverything() {
    let nodeList = document.getElementsByTagName("article");
    let i;
    for (i = 0; i < nodeList.length; i++) {
        store(nodeList[i].id + "textarea", document.getElementById(nodeList[i].id + "textarea").value);
        store(nodeList[i].id + "section", nodeList[i].parentElement.id);
    }
    for (i = 1; i < 1 + numColumns; i++) {
        let inputIdMaxArticles = "s" + i + "MaxArticles";
        store(inputIdMaxArticles, document.getElementById(inputIdMaxArticles).value);
        let inputIdTitle = "s" + i + "Title";
        store(inputIdTitle, document.getElementById(inputIdTitle).value);
    }
    updateRemainingSpaceInformation();

    // If we got here without error we clear the error text.
    document.getElementById('error').innerHTML = "";
}

function updateRemainingSpaceInformation() {
document.getElementById('space').innerHTML = "localStorage used space "
        + JSON.stringify(localStorage).length / 1000
        + " Kb. localStorage estimated remaining space "
        + ((5000000 - JSON.stringify(localStorage).length) / 1000)
        + " Kb.";
}

function store(key, value) {
    try {
        localStorage.setItem(key, value);
    } catch (error) {
        document.getElementById('error').innerHTML = "Could not finish saving all data. " + error;
        throw error;
    }
}

function isContainerAbleToFitMore(containerId) {
    // Check if the container is allowed to have another child element.
    let container = document.getElementById(containerId);
    if (container != null) {
        let max = localStorage.getItem(container.parentElement.id+"MaxArticles");
        if (max == null || max == "") {
            // No maximum has been set.
            return true;
        } else {
            let num = container.children.length;
            if (num >= max) {
                return false;
            }
            else {
                return true;
            }
        }
    }
    // The safe default is to allow fitting more.
    return true;
}

function addNewEntry(parentId) {

    if (isContainerAbleToFitMore(parentId)) {
        // We increment the articleCounter here;
        // that way if anything fails later on in this method we will never refer to that failed article again.
        articleCounter++;
        store("articleCounter", articleCounter);
        return addNumberedEntry(parentId, articleCounter);
    }
}
function addNumberedEntry(parentId, inputArticleCounter) {
    let parent = document.getElementById(parentId);
    let article = document.createElement("article");
    article.id = "article" + inputArticleCounter;
    article.draggable = true;
    article.ondrag = drag_handler;
    article.ondragstart = dragstart_handler;


    let textarea = document.createElement("textarea");
    textarea.id = article.id + "textarea"
    textarea.label = "Text input for action " + inputArticleCounter;
    textarea.rows = "1";

    let deleteButton = document.createElement("button");
    deleteButton.innerHTML = "delete forever";
    deleteButton.onclick = function() {
        localStorage.removeItem(article.id + "textarea")
        localStorage.removeItem(article.id + "section")
        deleteButton.parentElement.remove();
    }
    deleteButton.style = "float: right;"

    let copyButton = document.createElement("button");
    copyButton.innerHTML = "copy";
    copyButton.onclick = function() {
        copy(article.id);
    }
    copyButton.style = "float: right;";
    copyButton.classList.add("copyButton");

    let span = document.createElement("span");
    span.innerHTML = inputArticleCounter;
    span.style="font-size: 0.8em;"

    article.appendChild(textarea);
    article.appendChild(deleteButton);
    article.appendChild(copyButton);
    article.appendChild(span);
    parent.appendChild(article);

    let articleId = article.id;
    return articleId;
}

function copy(articleId) {
    article = document.getElementById(articleId);
    newArticleId = addNewEntry(article.parentElement.id);
    // newArticleId will be null if there is no more space in the column that contains the original
    if (newArticleId != null) {
        document.getElementById(newArticleId + "textarea").value = document.getElementById(articleId+"textarea").value;
    }
}

/***    On the draggable elements    ***/
function drag_handler(ev) {
     ev.target.style.background = colorForActiveElements;
}

function dragstart_handler(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

/***    On the targets    ***/
function dragenter_handler(ev) {
    if (isContainerAbleToFitMore(ev.currentTarget.id)) {
        ev.currentTarget.style.background = colorForActiveElements;
    }
    else {
        ev.currentTarget.style.background = colorForInactiveElements;
    }
}

function dragexit_handler(ev) {
    ev.currentTarget.style.background = colorForInactiveElements;
}

function dragover_handler(ev) {
    ev.preventDefault();
    if (isContainerAbleToFitMore(ev.currentTarget.id)) {
        ev.currentTarget.style.background = colorForActiveElements;
    }
    else {
        ev.currentTarget.style.background = colorForInactiveElements;
    }
}

function drop_handler(ev) {
    ev.preventDefault();

    let target = ev.target;
    while (target.draggable || target.parentElement.draggable) {
        target = target.parentElement;
    }

    let element = document.getElementById(ev.dataTransfer.getData("text"));
    if (isContainerAbleToFitMore(target.id)) {
        target.appendChild(element);
        target.style.background = colorForInactiveElements;
    }
    target.style.background = colorForInactiveElements;
    element.style.background = colorForInactiveElements;
}

function drop_disabled_handler(ev) {
    ev.preventDefault();
}

/*** Trashcan ***/
function minimizeTrashcan() {
    document.getElementById('trashcandropzone').classList.toggle('trashcanminimized');
    document.getElementById('trashcandropzone').classList.remove('trashcanmaximized');
}

function maximizeTrashcan() {
    document.getElementById('trashcandropzone').classList.toggle('trashcanmaximized');
    document.getElementById('trashcandropzone').classList.remove('trashcanminimized');
}

// https://stackoverflow.com/a/33542499
function createBackup() {
    let backup = JSON.stringify(localStorage);
    let filename = 'verticalboard-data-backup.json';
    var blob = new Blob([backup], {type: 'text/json'});
    if(window.navigator.msSaveOrOpenBlob) {
        // MS
        window.navigator.msSaveBlob(blob, filename);
    }
    else {
        var elem = window.document.createElement('a');
        elem.href = window.URL.createObjectURL(blob);
        elem.download = filename;
        document.body.appendChild(elem);
        elem.click();
        document.body.removeChild(elem);
        window.URL.revokeObjectURL(blob);
    }
}

async function loadBackup() {
    clearInterval(saveTimer);
    try {
        const file = document.getElementById('file').files[0];
        const loadText = file => new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsText(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
        });
        data = await loadText(file);
        Object.assign(localStorage,JSON.parse(data));

        // Remove all articles
        let nodeList = document.getElementsByTagName("article");
        let i;
        for (i = 0; i < nodeList.length; i++) {
            let node = nodeList[i];
            console.log("Removing " + node.id);
            document.getElementById(node.id).parentElement.removeChild(node);
        }

        loadFromLocalStorage();

    } catch (error) {
        // Start periodical save action again.
        saveTimer = setInterval("saveEverything()", 1000);

        document.getElementById('error_on_load_backup').innerHTML = "Could not load the backup data. " + error;
        throw error;
    }
}